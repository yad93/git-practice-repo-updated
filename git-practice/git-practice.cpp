// git-practice.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

static const char *INPUT_FILE = "input.txt";
static const char *OUTPUT_FILE = "output.txt";

template <typename T>
struct functor
{
    void operator()(T& e)
    {
        if (e%2 == 0) e /= 2;
		else e *= 2;
    }
};

template <typename T>
struct predicate
{
    T N;
	predicate(T val):N(val){} 
	predicate():N(5){} 
    bool operator()(T e)
    {
        return ( (e%N == 0) /*|| (value > N)*/ ); 
    }
};

template <typename T>
struct printer
{
    void operator()(T& e)
    {
        // not implemented
    }
};

template <typename T>
class input_reader 
{
public:
    std::vector<T> operator()(const char* filename)
    {
        std::vector<T> out;
		std::string line;
		std::ifstream file;
		T value;
		file.open(filename);

		if (file.is_open())
		{
			while (!file.eof())
			{
				file >> value;

				out.push_back(value);
			}
		}

		file.close();

        return out;
    }
};

template <typename T>
class output_writer
{
public:
    void operator()(const char* filename, std::vector<T>& vec)
    {
		std::ofstream file;

		file.open(filename);

		if (file.is_open())
		{
			for (auto i = 0; i < vec.size(); i++)
			{
				file << vec[i] << std::endl;
			}
		}

		file.close();
    }
};

int _tmain(int argc, _TCHAR* argv[])
{
    std::vector<int> source_v;

    using curr_type = std::remove_reference<decltype(source_v[0])>::type;

    std::vector<curr_type> target_v;

    input_reader<curr_type> read;
    source_v = read(INPUT_FILE);

    predicate<curr_type> pred;
    std::copy_if(source_v.begin(), source_v.end(), std::back_inserter(target_v), pred);

    functor<curr_type> func;
    std::for_each(target_v.begin(), target_v.end(), func);
    std::for_each(target_v.begin(), target_v.end(), func);

    printer<curr_type> printr;
    std::for_each(target_v.begin(), target_v.end(), printr);

    output_writer<curr_type> write;
    write(OUTPUT_FILE, target_v);

    return 0;
}
